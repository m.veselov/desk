var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var minify = require('gulp-minify');
var jade = require('gulp-jade');
var clean = require('gulp-clean');
var plumber = require('gulp-plumber');
var inject = require('gulp-inject');
var prefixer = require('gulp-autoprefixer');

gulp.task('watch',['browserSync','jade', 'sass'],function () {
  gulp.watch('app/templates/**/*.jade',['jade']);
  gulp.watch(['app/scss/*.scss','app/scss/modules/**/*.scss'], ['sass']);
  gulp.watch('public/*.html', browserSync.reload);

})
gulp.task('build',['useref','fonts'],function () {
  console.log("Builging files");
});

gulp.task('sass', function() {
  return gulp.src('app/scss/style.scss')
      .pipe(inject(gulp.src('app/scss/modules/**/*.scss', {read: false}), {
        starttag: '/* inject:imports */',
        endtag: '/* endinject */',
        transform: function (filepath) {
            return '@import ".' + filepath + '";';
        }
      }))
      .pipe(sass({
          outputStyle: 'compressed',
          includePaths: [
            'node_modules/susy/sass',
            'node_modules/node-normalize-scss',
            'node_modules/breakpoint-sass/stylesheets'
          ],
      }).on('error', sass.logError))
      .pipe(prefixer())
      .pipe(gulp.dest('public/css'))
      .pipe(browserSync.reload({
        stream:true
      }))
});



gulp.task('browserSync',function () {
  browserSync({
    server:{
      baseDir:'public'
    }
  })
});
gulp.task('useref', function(){
  return gulp.src('public/*.html')
    .pipe(useref())
    .pipe(gulpif('*.js', uglify()))
    .pipe(gulpif('*.css', minify()))
    .pipe(gulp.dest('public'))
});

gulp.task('jade', function () {
  return gulp.src(['app/templates/pages/*.jade', '!app/templates/base.jade'])
  .pipe(plumber())
  .pipe(jade({
      pretty: true
    }))
  .pipe(gulp.dest('public'))
})
gulp.task('fonts',function(){
  return gulp.src('app/fonts/**/*')
  .pipe(gulp.dest('public/fonts'))
})
gulp.task('clean-fonts', function () {
    return gulp.src('app/fonts/.gitkeep', {read: false})
        .pipe(clean());
});
gulp.task('clean-images', function () {
    return gulp.src('app/images/.gitkeep', {read: false})
        .pipe(clean());
});
gulp.task('fonts', function() {
  return gulp.src('app/fonts/**/*')
  .pipe(gulp.dest('public/fonts'))
})
gulp.task('img', function() {
  return gulp.src('app/images/**/*')
  .pipe(gulp.dest('public/images'))
})

gulp.task('js', function() {
  return gulp.src('app/js/**/*')
  .pipe(gulp.dest('public/js'))
})
gulp.task('clean', ['clean-fonts','clean-images']);
gulp.task('move', ['fonts','img', 'js']);

gulp.task('build', ['move','sass','jade']);